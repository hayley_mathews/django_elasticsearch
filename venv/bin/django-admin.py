#!/Users/hayley.mathews/django_elasticsearch/venv/bin/python3.5
from django.core import management

if __name__ == "__main__":
    management.execute_from_command_line()
